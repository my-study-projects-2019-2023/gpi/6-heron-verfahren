﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_HeronVerfahren
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Heron-Verfahren. 5 Näherungszahlen der Quadratwurzeln von den Zahlen 0 - 10.");
            for(double a = 0; a < 11; a++)
            {
                string quadratwurzelNaehrungenFuerA = "Die Quadratwurzel-Näherungen für " + a + " sind: ";
                for(double x = 1; x < 6; x++)
                {
                    quadratwurzelNaehrungenFuerA += ((x + (a / x)) / 2D);
                    if(x != 5) { quadratwurzelNaehrungenFuerA += ", "; }
                }
                Console.WriteLine(quadratwurzelNaehrungenFuerA);
            }
            Console.ReadKey();
        }
    }
}
